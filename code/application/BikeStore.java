// Jaypee Tello | #2232311
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String args[]) {
        Bicycle[] bicycle = new Bicycle[4];
        bicycle[0] = new Bicycle("BMW", 6, 200);
        bicycle[1] = new Bicycle("Lamborghini", 15, 600);
        bicycle[2] = new Bicycle("Ford", 8, 320);
        bicycle[3] = new Bicycle("Tesla", 4, 150);

        for(int i = 0; i < bicycle.length; i++) {
            System.out.println(bicycle[i]);
        }
    }
}
