// Jaypee Tello | #2232311
package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String newManufacturer, int newNumberGears, double newMaxSpeed) {
        this.manufacturer = newManufacturer;
        this.numberGears = newNumberGears;
        this.maxSpeed = newMaxSpeed;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public String toString() {
        String newString = ("Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed);
        return newString;
    }
}